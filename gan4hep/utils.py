import re
import numpy
import pandas
from scipy import stats
import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler

def shuffle(array):
    from numpy.random import MT19937
    from numpy.random import RandomState, SeedSequence
    np_rs = RandomState(MT19937(SeedSequence(123456789)))
    np_rs.shuffle(array)

def load_data(dimuon=False,filename=None,**kwargs):
    data = {}
    if dimuon:
        if filename==None:
            filename='/project/projectdirs/m3769/vincent/HEP/dimuon/mc16d_364100_100Kentries_dimuon.output'
        truth_data = numpy.loadtxt(filename).astype(numpy.float32)
        scaler = MinMaxScaler(feature_range=(-1,1))
        truth_in = scaler.fit_transform(truth_data)
        shuffle(truth_in)
        data['input_4vec'] = numpy.empty((len(truth_in),0))
        data['truth_in'] = truth_in
    else:
        if filename==None:
            filename='/project/projectdirs/m3769/vincent/HEP/ClusterTo2Pi0.dat'
        df = pandas.read_csv(filename, sep=';', header=None, names=None, engine='python')
        event = None
        with open(filename, 'r') as f:
            for line in f:
                event = line
                break
        particles = event[:-2].split(';')
        data['input_4vec'] = df[0].str.split(",", expand=True)[[4, 5, 6, 7]].to_numpy().astype(numpy.float32)
        out_particles = []
        for idx in range(1, len(particles)):
            out_4vec = df[idx].str.split(",", expand=True).to_numpy()[:, -4:].astype(numpy.float32)
            out_particles.append(out_4vec)
        out_4vec = out_particles[0]
        data['px'] = out_4vec[:, 1].astype(numpy.float32)
        data['py'] = out_4vec[:, 2].astype(numpy.float32)
        data['pz'] = out_4vec[:, 3].astype(numpy.float32)
        data['phi'] = numpy.arctan(data['px']/data['py'])
        data['theta'] = numpy.arctan(numpy.sqrt(data['px']**2 + data['py']**2)/data['pz'])
        data['truth_in'] = numpy.stack([data['phi'], data['theta']], axis=1)
    return data

def log_metrics(predict_4vec: numpy.ndarray, truth_4vec: numpy.ndarray):
    distances = []
    for icol in range(predict_4vec.shape[1]):
        dis = stats.wasserstein_distance(predict_4vec[:, icol], truth_4vec[:, icol])
        _, pvalue = stats.ks_2samp(predict_4vec[:, icol], truth_4vec[:, icol])
        if pvalue < 1e-7: pvalue = 1e-7
        energy_dis = stats.energy_distance(predict_4vec[:, icol], truth_4vec[:, icol])
        mse_loss = numpy.sum((predict_4vec[:, icol] - truth_4vec[:, icol])**2)/predict_4vec.shape[0]
        tf.summary.scalar("wasserstein_distance_var{}".format(icol), dis)
        tf.summary.scalar("energy_distance_var{}".format(icol), energy_dis)
        tf.summary.scalar("KS_Test_var{}".format(icol), pvalue)
        tf.summary.scalar("MSE_distance_var{}".format(icol), mse_loss)
        distances.append([dis, energy_dis, pvalue, mse_loss])
    distances = numpy.array(distances, dtype=numpy.float32)
    tot_wdis = sum(distances[:, 0]) / distances.shape[0]
    tot_edis = sum(distances[:, 1]) / distances.shape[0]
    tf.summary.scalar("tot_wasserstein_dis", tot_wdis, description="total wasserstein distance")
    tf.summary.scalar("tot_energy_dis", tot_edis, description="total energy distance")
    _ , comb_pvals = stats.combine_pvalues(distances[:, 2], method='fisher')
    tf.summary.scalar("tot_KS", comb_pvals, description="total KS pvalues distance")
    tot_mse = sum(distances[:, 3]) / distances.shape[0]
    tf.summary.scalar("tot_mse", tot_mse, description='mean squared loss')
    return tot_wdis, comb_pvals, tot_edis, tot_mse

def get_inference(path_to_model,batch_size,noise_dim,ds='test',n_events=5000,n_iter=1,debug=False,**kwargs):
    model      = tf.saved_model.load(path_to_model)
    data       = load_data(**kwargs)
    truth_in   = data['truth_in']
    test_in    = data['input_4vec'][:n_events] if ds=='test' else data['input_4vec'][n_events:]
    all_preds  = []
    tot_wdis   = []
    for i in range(n_iter):
        noise        = numpy.random.normal(loc=0., scale=1., size=(test_in.shape[0], noise_dim))
        test_input   = numpy.concatenate([test_in, noise], axis=1).astype(numpy.float32)
        test_truth   = truth_in[:n_events] if ds=='test' else truth_in[n_events:]
        testing_data = tf.data.Dataset.from_tensor_slices((test_input, test_truth)).batch(batch_size).prefetch(tf.data.experimental.AUTOTUNE)
        predictions  = []
        truths = []
        for data in testing_data:
            test_input, test_truth = data
            out = model(test_input, training=False)
            predictions.append(out)
            truths.append(test_truth)
        predictions = tf.concat(predictions, axis=0).numpy()
        truths = tf.concat(truths, axis=0).numpy()
        best_wdis = log_metrics(predictions, truths)[0]
        tot_wdis.append(best_wdis)
        all_preds.append(predictions)
        if debug:
            print('Iteration {:>4} / {}  |  best_wdis = {:.5f}'.format(i+1,n_iter,best_wdis))
    return tot_wdis,truths,numpy.array(all_preds)

def scatter_solutions(input_data,save=True,show=False,**kwargs):
    """
    Scatter plot of all evaluated HPO solutions. A top histogram is also displayed
    to show the distribution of losses found across all solutions. Multiple set of
    HPO solutions can be displayed next to each other, see examples.
    
    Parameters
    ----------
    input_data : :class:`list`
        Input extracted list of solutions.
    save : :class:`bool`
        Save the figure into a PDF with filename figure.pdf
    show : :class:`bool`
        Display figure.
        
    Examples
    --------
    A simple example of the ``input_data`` list can be as follows:
    
    >>> input_data = [solutions]
    
    where ``solutions`` is a set of solutions extracted from the saved log files
    using the :method:`hyppo.extract` function.
    
    
    More than one datasets can also be considered. For instance, let's consider two
    set of solutions, one, ``data1``, contains solutions obtained using Gaussian
    Process surrogate modeling, the other, ``data2``, are solutions obtained doing
    only random sampling, the ``input_data`` can be built as follows:
    
    >>> input_data = [[data1,'Gaussian Process'],[data2,'Random Sampling']]
    """
    plt.style.use('seaborn')
    fig, ax = plt.subplots(2,len(input_data),figsize=(4*len(input_data)+2,5),gridspec_kw={'height_ratios': [1, 3]},dpi=200,sharex=True,sharey='row')
    if len(input_data)==1:
        ax = numpy.array(ax,ndmin=2).T
    plt.subplots_adjust(left=0.05,top=0.93,right=0.9,wspace=0.05,hspace=0.1)
    for i,data in enumerate(input_data):
        if type(data)==list:
            data, title = data
            ax[0][i].set_title('(%i) %s' % (i+1,title))
        print('Dataset %i : %i solutions displayed' % (i+1,len(data)))
        ax[0][i].hist(data.loss,range=[0,1.6],rwidth=0.9,bins=16,histtype='bar')
        im = ax[1][i].scatter(data.loss,data.stdev,c=data.hp3*5e-5,lw=0.1,alpha=0.5,s=20,cmap='plasma')
        ax[1][i].minorticks_off()
        ax[1][i].set_xlabel('Best Wasserstein Distance')
        # ax[i].set_xlim([0,0.25])
        # ax[i].set_ylim([0,0.1])
        ax[1][i].scatter([0.125],[0.030], facecolors='none', edgecolors='tomato',s=1200,lw=2)
    ax[1][0].set_ylabel(r'1-$\mathrm{\sigma}$ Deviation')
    cax = plt.axes([0.91, 0.125, 0.02, 0.575])
    cbar = plt.colorbar(im,cax=cax,format='%.0e')
    cbar.set_alpha(1)
    cbar.draw_all()
    cbar.set_label('Generator Learning Rate')
    if save: plt.savefig('figure.pdf')
    if show: plt.show()
    plt.close()
