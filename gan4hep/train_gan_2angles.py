#!/usr/bin/env python

# Internal
import os
import math
import time
import logging

# External
import tqdm
import numpy
import pandas
import tensorflow as tf
from scipy import stats
from tensorflow import keras
from tensorflow.keras import layers
from sklearn.preprocessing import MinMaxScaler

from .utils import shuffle

class RunTraining():
    """
    This script trains a MLP-based GAN that generates `\phi` and `\theta'
    of the two outgoing particles.
    """
    def __init__(self,filename,noise_dim=4,num_test_evts=5000,batch_size=512,output_dir='log_training',max_evts=None,**kwargs):
        self.noise_dim = int(noise_dim)
        self.num_test_evts = int(num_test_evts)
        self.batch_size = int(batch_size)
        # Read input datasets
        if 'mc16d_364100_100Kentries_dimuon.output' in filename:
            self.application = 'dimuon'
            truth_data = numpy.loadtxt(filename).astype(numpy.float32)
            scaler = MinMaxScaler(feature_range=(-1,1))
            truth_in = scaler.fit_transform(truth_data)
            shuffle(truth_in)
            input_4vec = numpy.empty((len(truth_in),0))
        if 'ClusterTo2Pi0.dat' in filename:
            self.application = 'hadron'
            df = pandas.read_csv(filename, sep=';', header=None, names=None, engine='python')
            event = None
            with open(filename, 'r') as f:
                for line in f:
                    event = line
                    break
            particles = event[:-2].split(';')
            input_4vec = df[0].str.split(",", expand=True)[[4, 5, 6, 7]].to_numpy().astype(numpy.float32)
            out_particles = []
            for idx in range(1, len(particles)):
                out_4vec = df[idx].str.split(",", expand=True).to_numpy()[:, -4:].astype(numpy.float32)
                out_particles.append(out_4vec)
            out_4vec = out_particles[0]
            px = out_4vec[:, 1].astype(numpy.float32)
            py = out_4vec[:, 2].astype(numpy.float32)
            pz = out_4vec[:, 3].astype(numpy.float32)
            pT = numpy.sqrt(px**2 + py**2)
            phi = numpy.arctan(px/py)
            theta = numpy.arctan(pT/pz)
            truth_in = numpy.stack([phi, theta], axis=1)
        # the training data will be composed for each epoch
        self.train_in = input_4vec[self.num_test_evts:max_evts]
        self.train_truth = truth_in[self.num_test_evts:max_evts]
        # testing data
        test_in = input_4vec[:self.num_test_evts]
        noise = numpy.random.normal(loc=0., scale=1., size=(test_in.shape[0], self.noise_dim))
        test_in = numpy.concatenate([test_in, noise], axis=1).astype(numpy.float32)
        test_truth = truth_in[:self.num_test_evts]
        self.testing_data = tf.data.Dataset.from_tensor_slices(
            (test_in, test_truth)).batch(self.batch_size, drop_remainder=True)
        # Construct generator
        self.gen_input_dim = self.noise_dim + input_4vec.shape[1]
        # Write up summary
        self.output_dir = output_dir
        summary_dir = os.path.join(self.output_dir, "logs")
        self.summary_writer = tf.summary.create_file_writer(summary_dir)

    def build(self,lr_gen=1e-4,lr_disc=1e-4,gen_hidden_layer=1,gen_layersize=256,disc_hidden_layer=1,disc_layersize=256,**kwargs):
        # Create directory to save images
        self.gen_dir = os.path.join(self.output_dir, 'generator')
        os.makedirs(self.gen_dir, exist_ok=True)
        self.AUTO = tf.data.experimental.AUTOTUNE
        # testing data
        self.testing_data = self.testing_data.prefetch(self.AUTO)
        gen_output_dim = self.train_truth.shape[1]
        self.generator = keras.Sequential(
            [
                keras.Input(shape=(self.gen_input_dim,)),
                layers.Dense(gen_layersize),
                layers.BatchNormalization(),
                layers.LeakyReLU()
            ] +
            [
                layers.Dense(gen_layersize),
                layers.BatchNormalization()
            ]*gen_hidden_layer +
            [
                layers.Dense(gen_output_dim),
                layers.Activation("tanh"),
            ] + ([layers.Lambda(lambda x: x * numpy.float32(math.pi / 2))] if self.application=='hadron' else [])
        )
        self.generator.summary()
        self.discriminator = keras.Sequential(
            [
                keras.Input(shape=(gen_output_dim,)),
                layers.Dense(disc_layersize),
                layers.BatchNormalization(),
                layers.LeakyReLU()
            ] +
            [
                layers.Dense(disc_layersize),
                layers.BatchNormalization(),
                layers.LeakyReLU(),
            ]*disc_hidden_layer +
            [
                layers.Dense(1, activation='sigmoid'),
            ]
        )
        self.discriminator.summary()
        logging.info('-'*40)
        ngen = numpy.sum([numpy.prod(v.get_shape()) for v in self.generator.trainable_weights])
        ndis = numpy.sum([numpy.prod(v.get_shape()) for v in self.discriminator.trainable_weights])
        logging.info('{:,d} parameters in generator'.format(ngen))
        logging.info('{:,d} parameters in discriminator'.format(ndis))
        logging.info('Number of parameters : {:,d}'.format(ngen+ndis))
        logging.info('-'*40)
        self.cross_entropy = keras.losses.BinaryCrossentropy(from_logits=False)
        # Specify optimizer
        self.generator_optimizer = keras.optimizers.Adam(lr_gen)
        self.discriminator_optimizer = keras.optimizers.Adam(lr_disc)
        # Set up checkpoint savings
        checkpoint_dir = os.path.join(self.output_dir, "checkpoints")
        checkpoint = tf.train.Checkpoint(generator_optimizer=self.generator_optimizer,
                                        discriminator_optimizer=self.discriminator_optimizer,
                                        generator=self.generator,
                                        discriminator=self.discriminator)
        self.ckpt_manager = tf.train.CheckpointManager(checkpoint, checkpoint_dir, max_to_keep=None)
        _ = checkpoint.restore(self.ckpt_manager.latest_checkpoint).expect_partial()
    
    def discriminator_loss(self,real_output, fake_output):
        real_loss = self.cross_entropy(tf.ones_like(real_output), real_output)
        fake_loss = self.cross_entropy(tf.zeros_like(fake_output), fake_output)
        total_loss = real_loss + fake_loss
        return tf.reduce_mean(total_loss)

    def generator_loss(self,fake_output):
        return tf.reduce_mean(self.cross_entropy(tf.ones_like(fake_output), fake_output))

    def train(self,epochs=50,**kwargs):
        best_epoch = 0
        self.best_wdis = 900
        best_list = []
        for epoch in tqdm.trange(epochs):
            start = time.time()
            # compose the training dataset by generating different noises
            noise = numpy.random.normal(loc=0., scale=1., size=(self.train_in.shape[0], self.noise_dim))
            train_inputs = numpy.concatenate([self.train_in, noise], axis=1).astype(numpy.float32)
            self.dataset = tf.data.Dataset.from_tensor_slices((train_inputs, self.train_truth)).batch(self.batch_size, drop_remainder=True).prefetch(self.AUTO)
            tot_loss = []
            for data_batch in self.dataset:
                tot_loss.append(list(self.train_step(*data_batch)))
            tot_loss = numpy.array(tot_loss)
            avg_loss = numpy.sum(tot_loss, axis=0)/tot_loss.shape[0]
            loss_dict = dict(disc_loss=avg_loss[0], gen_loss=avg_loss[1])
            tot_wdis = self.calculate_wdist(epoch+1, **loss_dict)[0]
            logging.info('Epoch {:>3} / {:<3} {:>12} {:>11.5f}'.format(epoch+1,epochs,'W_dist :',tot_wdis))
            best_list.append([tot_wdis,epoch+1])
            if tot_wdis < self.best_wdis:
                self.ckpt_manager.save()
                self.best_wdis = tot_wdis
                gen_dir = self.gen_dir #os.path.join(self.gen_dir, 'gen_at_epoch_{:04d}'.format(epoch+1))
                if os.path.exists(gen_dir):
                    os.system('rm -rf %s' % gen_dir)
                self.generator.save(gen_dir)
        best_list.sort()
        numpy.savetxt(os.path.join(self.output_dir, "best.txt"),best_list,fmt='%.5f %i')

    @tf.function
    def train_step(self, gen_in_4vec, truth_4vec):
        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            gen_out_4vec = self.generator(gen_in_4vec, training=True)
            fake_output = self.discriminator(gen_out_4vec, training=True)
            real_output = self.discriminator(truth_4vec, training=True)
            gen_loss = self.generator_loss(fake_output)
            disc_loss = self.discriminator_loss(real_output, fake_output)
        gradients_of_generator = gen_tape.gradient(gen_loss, self.generator.trainable_variables)
        gradients_of_discriminator = disc_tape.gradient(disc_loss, self.discriminator.trainable_variables)
        self.generator_optimizer.apply_gradients(zip(gradients_of_generator, self.generator.trainable_variables))
        self.discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator, self.discriminator.trainable_variables))
        return disc_loss, gen_loss

    def calculate_wdist(self, epoch=0, **kwargs):
        predictions = []
        truths = []
        for data in self.testing_data:
            test_input, test_truth = data
            predictions.append(self.generator(test_input, training=False))
            truths.append(test_truth)
        predictions = tf.concat(predictions, axis=0).numpy()
        truths = tf.concat(truths, axis=0).numpy()
        tot_wdis = self.log_metrics(predictions, truths, epoch, summary=True, **kwargs)[0]
        return tot_wdis, truths, predictions
    
    def log_metrics(self,predict_4vec,truth_4vec,step=0,**kwargs):
        with self.summary_writer.as_default():
            tf.summary.experimental.set_step(step)
            # plot the eight variables and resepctive Wasserstein distance (i.e. Earch Mover Distance)
            # Use the Kolmogorov-Smirnov test, 
            # it turns a two-sided test for the null hypothesis that the two distributions
            # are drawn from the same continuous distribution.
            # https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.combine_pvalues.html#scipy.stats.combine_pvalues
            # https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.ks_2samp.html#scipy.stats.ks_2samp
            # https://docs.scipy.org/doc/scipy/reference/stats.html#statistical-distances
            distances = []
            for icol in range(predict_4vec.shape[1]):
                dis = stats.wasserstein_distance(predict_4vec[:, icol], truth_4vec[:, icol])
                _, pvalue = stats.ks_2samp(predict_4vec[:, icol], truth_4vec[:, icol])
                if pvalue < 1e-7: pvalue = 1e-7
                energy_dis = stats.energy_distance(predict_4vec[:, icol], truth_4vec[:, icol])
                mse_loss = numpy.sum((predict_4vec[:, icol] - truth_4vec[:, icol])**2)/predict_4vec.shape[0]
                tf.summary.scalar("wasserstein_distance_var{}".format(icol), dis)
                tf.summary.scalar("energy_distance_var{}".format(icol), energy_dis)
                tf.summary.scalar("KS_Test_var{}".format(icol), pvalue)
                tf.summary.scalar("MSE_distance_var{}".format(icol), mse_loss)
                distances.append([dis, energy_dis, pvalue, mse_loss])
            distances = numpy.array(distances, dtype=numpy.float32)
            tot_wdis = sum(distances[:, 0]) / distances.shape[0]
            tot_edis = sum(distances[:, 1]) / distances.shape[0]
            tf.summary.scalar("tot_wasserstein_dis", tot_wdis, description="total wasserstein distance")
            tf.summary.scalar("tot_energy_dis", tot_edis, description="total energy distance")
            _ , comb_pvals = stats.combine_pvalues(distances[:, 2], method='fisher')
            tf.summary.scalar("tot_KS", comb_pvals, description="total KS pvalues distance")
            tot_mse = sum(distances[:, 3]) / distances.shape[0]
            tf.summary.scalar("tot_mse", tot_mse, description='mean squared loss')
            if kwargs is not None:
                # other metrics to be recorded.
                for key,val in kwargs.items():
                    tf.summary.scalar(key, val)
        return tot_wdis, comb_pvals, tot_edis, tot_mse
    
def run_training(**kwargs):
    gan = RunTraining(**kwargs)
    gan.build(**kwargs)
    gan.train(**kwargs)
    return gan.best_wdis
