from setuptools import find_packages
from setuptools import setup

description="Use GAN to generate particle physics events"

setup(
    name="gan4hep",
    version="0.3.0",
    description=description,
    long_description=description,
    author="Xiangyang Ju and Vincent Dumont",
    license="Apache License, Version 2.0",
    keywords=["GAN", "HEP"],
    url="https://gitlab.com/hpo-uq/applications/gan4hep",
    packages=find_packages(),
    install_requires=[
        'tensorflow >= 2.0.0',
        "matplotlib",
        'tqdm',
        'scipy',
        'scikit-learn',
    ],
    classifiers=[
        "Programming Language :: Python :: 3.8",
    ]
)

